#!/bin/bash
#    .---------- constant part!
#    vvvv vvvv-- the code from above
RED='\033[0;31m'
NC='\033[0m' # No Color
# Repara la base de datos mongo en el caso de un apagado 
# Dando el error Detected unclean shutdown
docker system prune -a -f
# printf "${RED}Bajar servidores${NC}\n" 

  docker-compose down
printf "${RED}Reparar${NC} Mongo\n" 
  docker-compose run mongo mongod --repair
printf "${RED}Subir contenedores${NC}\n" 
   docker-compose up --build -d

# # Crea el usuario root y el password example en mongo como root admin
# crearAdminEnMongo(){
#   docker exec -it mongo mongo admin --eval "db.createUser({user: 'root', pwd: 'example', roles:[{role:'root',db:'admin'}]});"
# }
# printf "${RED}Crear admin${NC} Mongo\n" 
# crearAdminEnMongo
