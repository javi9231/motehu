# MOTEHU

_Monitor de temperatura y humedad_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


## Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_
## Portainer
_Para gestionar los contenedores visualmente es mejor utilizar portainer. La manera más fácil es levantar un contenedor._
```
sudo docker run -d --name Portainer --restart=always -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```


# Instalación 🔧

## Clonar el repositorio 
```
git clone 
```

# Despliegue
## Ejecutar el comando de docker compose
Este comando baja los servidores si ya están iniciados, arregla la BBDD si se ha apagado inesperadamente y levanta los contenedores con un nuevo build
```
docker-compose down && docker-compose run mongo mongod --repair && docker-compose up --build -d
```

## Crear el usuario ADMIN en MongoDB
Al terminar el comando anterior, hay que crear un usuario para acceder a la base de datos, con el siguiente comando
```
docker exec -it mongo mongo admin --eval "db.createUser({user: 'root', pwd: 'example', roles:[{role:'root',db:'admin'}]});"
```
## Posibles errores
### MongoDb
_Ante un reinicio no esperado, la base de datos mongoDb puede verse afectada por errores, por lo que es conveniente ejecutar este comando_
```
docker-compose down && docker-compose run mongo mongod --repair && docker-compose up --build -d
```
```
docker exec -it mongo mongo admin --eval "db.createUser({user: 'root', pwd: 'example', roles:[{role:'root',db:'admin'}]});"
```

## Construido con 🛠️

* [Nodejs](https://nodejs.org/) - Servidor del backend
* [ReactJs](https://es.reactjs.org/) - Frontend
* [MongoDb](https://www.mongodb.com/) - Como base de datos
* [Docker](https://www.docker.com/) - Gestión de contenedores
* [MQTT](https://mqtt.org/) - Protocolo de comunicación por mensajes
* [Mosquitto](https://mosquitto.org/) - MQTT broker




## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/villanuevand/xxxxxx) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Andrés Villanueva** - *Trabajo Inicial* - [villanuevand](https://github.com/villanuevand)
* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.


