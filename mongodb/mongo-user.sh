#!/bin/bash
sudo docker run -d --name Portainer --restart=always -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
docker-compose down && docker-compose run mongo mongod --repair && docker-compose up --build -d
docker exec -it mongo mongo admin --eval "db.createUser({user: 'root', pwd: 'example', roles:[{role:'root',db:'admin'}]});"
