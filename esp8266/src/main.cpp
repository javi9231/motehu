//Ejemplo MQTT & ESP32 con sensor de temperatura DHT22 by Biblioman
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHTesp.h"
#include <stdlib.h>

String model = "esp32";
String clientId = "";
const char *ssid = "NETGEAR";
const char *password = "JpL.2020";
const char *mqttServer = "193.168.0.15";
const int mqttPort = 1883;
const int INTERVALO_MEDIDAS = 30000;
// const char *mqttUser = "dqyoxjgo";
// const char *mqttPassword = "bDFPyIOx5Mln";
String nodo = "";

WiFiClient espClient;
PubSubClient client(espClient);
DHTesp dht;

void enviaDatosMQTT(char *nodoNombre, const char *valorNodo)
{
  String nodoPublish = nodo + "/" + nodoNombre;
  client.publish(nodoPublish.c_str(), valorNodo);
  printf((nodoPublish + "/" + valorNodo).c_str());
}

void setup()
{

  Serial.begin(9600);
  WiFi.begin(ssid, password);
  dht.setup(5, DHTesp::DHT22);

  Serial.print("Periodo minimo de muestreo: ");
  Serial.println(dht.getMinimumSamplingPeriod());

  Serial.println("Conectado a la red WiFi");

  client.setServer(mqttServer, mqttPort);

  clientId = model + "/" + ESP.getChipId();

  Serial.print("Cliente id : ");
  Serial.print(clientId);
  nodo = "motehu/" + clientId;
}

void reconectarMQTT()
{
  while (!client.connected())
  {
    Serial.println("Conectando a Broquer MQTT...");

    if (client.connect(clientId.c_str()))
    {
      Serial.println("conectado");
      enviaDatosMQTT("conectado", clientId.c_str());
    }
    else
    {

      Serial.print("conexion fallida ");
      Serial.print(client.state());
      yield();
    }
  }
}

void reconectarWIFI()
{
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    WiFi.reconnect();
    Serial.println("Conectando a red WiFi...");
    yield();
  }
}

void loop()
{
  delay(dht.getMinimumSamplingPeriod());
  float temperatura = dht.getTemperature();
  float humedad = dht.getHumidity();
  String jsonS = "{\"temp\": " + String(temperatura, 0) + ",\"hum\":" + String(humedad, 0) + "}";
  client.publish((nodo + "/medicion").c_str(), jsonS.c_str());
  Serial.println(jsonS);
  delay(INTERVALO_MEDIDAS);
  if (!client.connected())
  {
    reconectarMQTT();
  }
  Serial.println(clientId);
  client.loop();
}
