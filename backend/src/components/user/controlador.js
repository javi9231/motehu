import User from './modelo';
import services from '../../services';

const signUp = (req, res) => {
  const user = new User({
    email: req.body.email,
    nombre: req.body.nombre,
    password: req.body.password,
  });

  user.save((err) => {
    if (err)
      res.status(500).send({ message: `Error al crear el usuario: ${err}` });
    return res.status(201).send({ token: services.createToken(user) });
  });
};

const signIn = (req, res) => {
  User.find({ email: req.body.email }, (err, user) => {
    if (err) return res.status(500).send({ message: err });
    if (!user) return res.status(404).send({ message: 'No existe el usuario' });

    req.user = user;
    res.status(200).send({
      message: 'Te has logeado correctamente',
      token: services.createToken(user),
    });
  });
};
export default { signUp, signIn };
