import { Router } from 'express';
const router = Router();

router.get('/', (req, res) => {
  res.status(200).send({
    message: 'usuario',
  });
});
router.get('/private', (req, res) => {
  res.status(200).send({
    message: 'Tienes acceso',
  });
});

export default router;
