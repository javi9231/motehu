import Medicion from './modelo';

// Crear
const crear = ({ idDispositivo, ubicacion, temperatura, humedad }) => {
  const medicion = new Medicion({
    idDispositivo,
    ubicacion,
    temperatura,
    humedad,
  });

  return medicion.save();
};

// Leer
const leerTodos = (req, res, next) => {
  Medicion.find({})
    .then((r) => res.status(200).send(r))
    .catch(next);
};
const leerIdDispositivo = (req, res, next) => {
  Medicion.find({
    id: req.params.id,
    createdAt: { $gte: req.params.fechaInicio, $lt: req.params.fechaFin },
  })
    .then((r) => res.status(200).send(r))
    .catch(next);
};
const leerNombreUbicacion = (req, res, next) => {
  Medicion.find({
    ubicacion: req.params.nombreUbicacion,
    createdAt: { $gte: req.params.fechaInicio, $lt: req.params.fechaFin },
  })
    .then((r) => res.status(200).send(r))
    .catch(next);
};
const leerNombreUbicacion_idDispositivo = (req, res, next) => {
  const params = req.params;
  Medicion.find({
    ubicacion: params.nombreUbicacion,
    idDispositivo: params.idDispositivo,
    createdAt: {
      $gte: params.fechaInicio,
      $lt: params.fechaFin,
    },
  })
    .then((r) => res.status(200).send(r))
    .catch(next);
};

const leerDesdeUnaFecha = (req, res, next) => {
  Medicion.find({ createdAt: { $gt: req.params.createdAt } });
  return;
};
const leer = {
  todos: leerTodos,
  idDispositivo: leerIdDispositivo,
  nombreUbicacion: leerNombreUbicacion,
  nombreUbicacion_idDispositivo: leerNombreUbicacion_idDispositivo,
  fecha: leerDesdeUnaFecha,
};
export default { crear, leer };
