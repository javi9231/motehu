import { Router } from 'express';
import medicion from './controlador';
var router = Router();

// Leer
router.get('/all', medicion.leer.todos);
router.get(
  '/idDispositivo/:idDispositivo/:fechaInicio/:fechaFin',
  medicion.leer.idDispositivo,
);
router.get(
  '/ubicacion/:nombreUbicacion/:fechaInicio/:fechaFin',
  medicion.leer.nombreUbicacion,
);
router.get(
  '/ubicacion_idDispositivo/:nombreUbicacion/:idDispositivo/:fechaInicio/:fechaFin',
  medicion.leer.nombreUbicacion_idDispositivo,
);

export default router;
