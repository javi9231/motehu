import { Schema, model } from 'mongoose';

const MedicionSchema = new Schema(
  {
    idDispositivo: {
      type: String,
      required: true,
    },
    ubicacion: {
      type: String,
    },
    temperatura: {
      type: String,
    },
    humedad: {
      type: String,
    },
  },
  { timestamps: true },
);

const Medicion = model('Medicion', MedicionSchema);

export default Medicion;
