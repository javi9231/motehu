import { Router } from 'express';
import { leer, crear, actualizar, borrar } from './controlador';
const router = Router();

// Crear
router.post('/crear', crear.ubicacion);
// Leer
router.get('/all', leer.todos);
router.get('/nombre/:nombre', leer.nombre);
// Actualizar
router.put('/:nombre', actualizar.ubicacion);
// Borrar
router.delete('/:nombre', borrar.nombre);
export default router;
