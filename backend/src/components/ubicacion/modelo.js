import { Schema, model } from 'mongoose';

const UbicacionSchema = new Schema(
  {
    nombre: {
      type: String,
      required: true,
      unique: true,
    },
    dispositivos: [],
    activa: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
);

const Ubicacion = model('Ubicacion', UbicacionSchema);

export default Ubicacion;
