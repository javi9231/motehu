import Ubicacion from './modelo';
import cambiarDispositivoDeUbicacion from '../../services/cambiarDispositivoDeUbicacion';
import avisarFrontActualizarDatos from '../../services/avisarFrontActualizarDatos';

// Create
const crearUbicacion = async (req, res, next) => {
  console.log('crearUbicacion');
  const dispositivos = req.body.dispositivosUbicacion;
  const nombreUbicacion = req.body.nombreUbicacion;
  try {
    if (!nombreUbicacion || !dispositivos)
      throw new Error({ message: 'NO EXISTEN NOMBRE' });

    const existeUbicacion = await Ubicacion.findOne({
      nombre: nombreUbicacion,
    });
    if (!existeUbicacion) {
      const ubicacion = await new Ubicacion({
        nombre: nombreUbicacion,
      });
      await ubicacion.save();
    }

    // add dispositivos
    dispositivos.forEach(async (idDispositivo) => {
      await cambiarDispositivoDeUbicacion({
        idDispositivo,
        nombreUbicacion,
      });
    });
    res.status(200).send({ message: 'OK' });
  } catch (error) {
    res
      .status(400)
      .send({ message: 'No se ha podido crear la ubicación' }, error);
  }
};

const crearUbicacionInicial = async ({ nombre, dispositivos }) => {
  console.log('crearUbicacion');
  let ubicacion = await Ubicacion.findOne({ nombre });
  if (!ubicacion) {
    ubicacion = new Ubicacion({
      nombre,
      dispositivos,
    });
  } else {
    ubicacion.activa = true;
    ubicacion.dispositivos.push(...dispositivos);
  }
  return ubicacion.save();
};

// Read
const leerTodos = (req, res, next) => {
  Ubicacion.find({})
    .then((r) => res.status(200).send(r))
    .catch((error) => console.log('error leer ', error));
};
const leerNombre = (req, res, next) => {
  Ubicacion.findOne({ nombre: req.params.nombre })
    .then((r) => res.status(200).send(r))
    .catch(next);
};

// Update
const actualizarUbicacion = async (req, res, next) => {
  const nombre = req.params.nombre;
  const ubicacion = await Ubicacion.findOne({ nombre });
  if (!ubicacion) {
    res.status(400).send({
      message: `No se ha encontrado la ubicación ${nombre}`,
    });
    return;
  }
  ubicacion.nombre = req.body.nombre || nombre;
  await ubicacion.save();

  const nuevosDispositivos = req.body.dispositivos;

  // si se mandan dispositivos
  if (nuevosDispositivos.length > 0) {
    // // quitamos los dispositivos que no van en el body
    // if (nuevosDispositivos) {
    //   const sinDuplicados = new Set(ubicacion.dispositivos);
    //   const sinDuplicadosArray = [...Array.from(sinDuplicados)];

    //   await Ubicacion.updateOne(
    //     { nombre: ubicacion.nombre },
    //     { dispositivos: sinDuplicadosArray },
    //   );
    // }
    // buscamos si ya esta el dispositivo asignado
    // si no esta asignado lo asignamos
    // let dispositivosSet = new Set();
    nuevosDispositivos.forEach(async (d) => {
      // dispositivosSet.add(d);
      await cambiarDispositivoDeUbicacion({
        idDispositivo: d,
        nombreUbicacion: ubicacion.nombre,
      });
    });
  }

  res.status(200).send({
    message: `Actualizada la ubicación ${req.params.nombre} pasa a ser ${req.body.nombre}`,
  });

  avisarFrontActualizarDatos({
    payload: {
      ubicacion: true,
      dispositivos: true,
    },
  });
};

// Delete
const borrarNombre = (req, res, next) => {
  res.status(200).send({ message: `Borrar ${req.params.nombre}` });
};

const crear = {
  ubicacion: crearUbicacion,
  ubicacionInicial: crearUbicacionInicial,
};
const leer = {
  todos: leerTodos,
  nombre: leerNombre,
};

const actualizar = {
  ubicacion: actualizarUbicacion,
};

const borrar = { nombre: borrarNombre };

export { crear, leer, actualizar, borrar };
