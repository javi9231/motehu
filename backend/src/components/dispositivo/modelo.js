import { Schema, model } from 'mongoose';

const DispositivoSchema = new Schema(
  {
    idDispositivo: {
      type: String,
      unique: true,
      required: true,
    },
    modelo: {
      type: String,
    },
    nombre: {
      type: String,
      unique: true,
    },
    ubicacion: {
      type: String,
    },
  },
  { timestamps: true },
);

const Dispositivo = model('Dispositivo', DispositivoSchema);

export default Dispositivo;
