import { Router } from 'express';
import { leer, actualizar, borrar } from './controlador';
var router = Router();

// Crear
// router.post('/crear', crear.dispositivo);
// Leer
router.get('/all', leer.todos);
router.get('/id/:id', leer.idDispositivo);
router.get('/nombre/:nombre', leer.nombre);
// Actualizar
router.put('/nombre', actualizar.nombre);
router.put('/ubicacion', actualizar.ubicacion);
// Borrar
router.delete('/id', borrar.idDispositivo);
router.delete('/nombre', borrar.nombre);

export default router;
