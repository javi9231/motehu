import Dispositivo from './modelo';
// CRUD

// Create
const crearDispositivo = async (idDispositivo, modelo) => {
  console.log('crearDispositivo');
  const dispositivo = new Dispositivo({
    idDispositivo: idDispositivo,
    modelo,
    nombre: idDispositivo,
    ubicacion: idDispositivo,
  });
  return dispositivo.save();
};

// Read
const leerTodos = (req, res, next) => {
  Dispositivo.find({})
    .then((r) => res.status(200).send(r))
    .catch(next);
};
const leerIdDispositivo = (req, res, next) => {
  Dispositivo.findOne({ idDispositivo: req.params.idDispositivo })
    .then((r) => res.status(200).send(r))
    .catch(next);
};
const leerNombre = (req, res, next) => {
  Dispositivo.find({ nombre: req.params.nombre })
    .then((r) => res.status(200).send(r))
    .catch(next);
};

// Update

const actualizarNombre = (req, res, next) => {
  const [idDispositivo, nombre] = req.body;
  Dispositivo.findOneAndUpdate(
    { idDispositivo: idDispositivo },
    { nombre: nombre },
  );
  res.status(200).send({
    message: `${idDispositivo} Actualizado el nombre a ${nombre}`,
  });
};
const actualizarUbicacion = async (req, res, next) => {
  // const [idDispositivo, ubicacion] = req.body;
  const dispositivo = await Dispositivo.findOne({
    idDispositivo: req.body.idDispositivo,
  });
  dispositivo.ubicacion = req.body.ubicacion;
  dispositivo
    .save()
    .then((dis) =>
      res
        .status(200)
        .send({ message: `Actualizada la ubicacion ${dis.ubicacion}` }),
    )
    .catch((err) => console.error('error al actualizar ubicacion: ', err));
};
// Delete
const borrarIdDispositivo = (req, res, next) => {
  res.status(200).send({ message: `Borrar Id ${req.params.idDispositivo}` });
};
const borrarNombre = (req, res, next) => {
  res.status(200).send({ message: `Borrar ${req.params.nombre}` });
};

const crear = {
  dispositivo: crearDispositivo,
};
const leer = {
  todos: leerTodos,
  nombre: leerNombre,
  idDispositivo: leerIdDispositivo,
};

const actualizar = {
  nombre: actualizarNombre,
  ubicacion: actualizarUbicacion,
};

const borrar = {
  idDispositivo: borrarIdDispositivo,
  nombre: borrarNombre,
};

export { crear, leer, actualizar, borrar };
