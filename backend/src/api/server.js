import createError from 'http-errors';
import { json, urlencoded } from 'express';
import logger from 'morgan';
import routes from './routes';
import dotenv from 'dotenv';
import userCtrl from '../components/user/controlador';
import moment from 'moment';
import handleErrors from './middleware/handleErrors';
import auth from './middleware/auth';

dotenv.config();
const PORT = process.env.APP_PORT || 8081;
const API_VERSION = 'v1';

const server = (app) => {
  app.use(logger('dev'));

  // Configurar cabeceras y cors
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method',
    );
    res.header(
      'Access-Control-Allow-Methods',
      'GET, POST, OPTIONS, PUT, DELETE',
    );
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
  });

  app.use(
    urlencoded({
      extended: false,
    }),
  );
  app.use(json());

  // Muestra por consola el día
  app.use((req, res, next) => {
    console.log('Time:', moment().utc().format('DD-MM-YYYY HH:mm:ss'));
    next();
  });

  // sign in
  app.post('/signin', userCtrl.signIn);
  // sign up
  app.post('/signup', userCtrl.signUp);

  // ruta de usuarios
  app.use(`/api/${API_VERSION}/user`, auth.isAuth, routes.usersRouter);
  // ruta de dispositivos
  app.use(`/api/${API_VERSION}/dispositivo`, routes.dispositivosRouter);
  // ruta ubicacion
  app.use(`/api/${API_VERSION}/ubicacion`, routes.ubicacionRouter);
  // ruta ubicacion
  app.use(`/api/${API_VERSION}/medicion`, routes.medicionRouter);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404, 'Qué estás buscando??'));
  });

  // error handler
  app.use(handleErrors);

  const onStart = () => {
    console.log('El servidor motehu esta corriendo en el puerto:', PORT);
  };
  app.listen(PORT, onStart);
};
export default server;
