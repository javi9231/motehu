import usersRouter from '../../components/user/router';
import dispositivosRouter from '../../components/dispositivo/router';
import ubicacionRouter from '../../components/ubicacion/router';
import medicionRouter from '../../components/medicion/router';

export default {
  usersRouter,
  dispositivosRouter,
  ubicacionRouter,
  medicionRouter,
};
