import { connect } from 'mqtt';
import nuevaMedicion from '../services/nuevaMedicion';

const mqttConexion = () => {
  // clientId unica
  const ip = process.env.MQTT_URL;
  const options = {
    protocol: process.env.MQTT_PROTOCOL,
    // keepalive: 30,
    port: process.env.MQTT_PORT,
  };

  options.clientId = `mqttjs_backend_${Math.random()
    .toString(16)
    .substr(2, 8)}`;
  return connect(ip, options);
};

const conexionesMQTT = ({ clientMQTT }) => ({
  nuevaMedicion: () => mqttNuevaMedicion({ clientMQTT }),
  enviarDatos: ({ topic, payload }) =>
    mqttEnviarDatos({ clientMQTT })({ topic, payload }),
});
const mqttNuevaMedicion = ({ clientMQTT }) => {
  if (!clientMQTT) return;
  clientMQTT.on('connect', function () {
    console.log('connected mqtt');
    clientMQTT.subscribe('motehu/#');
  });

  clientMQTT.on('message', (topicMqtt, messageMqtt) => {
    const [, modelo, id, accion] = topicMqtt.split('/');
    let mensaje = null;
    console.log('topic: ', topicMqtt);
    try {
      if (accion !== 'medicion') return;
      mensaje = JSON.parse(messageMqtt.toString());
      nuevaMedicion(modelo, id, mensaje);
    } catch (error) {
      mensaje = { error, mensaje: messageMqtt.toString() };
    }

    console.log(modelo, id, accion, mensaje);
  });
};

const mqttEnviarDatos =
  ({ clientMQTT }) =>
    async ({ topic, payload }) => {
      if (!clientMQTT) return;
      try {
        await clientMQTT.publish(topic, JSON.stringify(payload));
      } catch (error) {
        throw new Error('No se han podido mandar los datos ', error);
      }
    };
const mqttController = {
  conexion: mqttConexion,
  conexionesMQTT,
};
export default mqttController;
