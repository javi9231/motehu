import { connect, connection } from 'mongoose';
import { config } from 'dotenv';

const checkConnection = () => {
  return connection.readyState;
};

const connectMongoDb = () => {
  config();
  const uri = process.env.MONGO_DB_URL;
  if (!checkConnection()) {
    connect(uri, {
      dbName: process.env.MONGO_DB_NAME,
      user: process.env.MONGO_DB_USERNAME,
      pass: process.env.MONGO_DB_PASSWORD,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
      .then(() => {
        console.log('Connection estabislished with MongoDB');
      })
      .catch((error) => console.error(error.message));
  }
};

connection.on('connected', () => {
  console.log('Mongoose connected to DB Cluster');
});

connection.on('error', (error) => {
  console.error('error conexión mongo', error.message);
  connectMongoDb();
});

connection.on('disconnected', () => {
  console.log('Mongoose Disconnected');
  connectMongoDb();
});

export default {
  connectMongoDb,
  checkConnection,
};
