import dotenv from 'dotenv';
import express from 'express';
import server from '../api/server';
import mongoCtrl from '../mongo/mongoCtrl';
import mqttController from '../mqtt/mqttController';

dotenv.config();
mongoCtrl.connectMongoDb();
// Creamos servidor http
const app = server(express());

// Conectamos a mqtt
const conexionMqtt = () => mqttController.conexion();
mqttController.conexionesMQTT({ clientMQTT: conexionMqtt() }).nuevaMedicion();
export default app;
