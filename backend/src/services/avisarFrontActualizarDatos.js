import mqttController from '../mqtt/mqttController';

const avisarFrontActualizarDatos = ({ payload }) => {
  const conexionMqtt = () => mqttController.conexion();
  mqttController.conexionesMQTT({ clientMQTT: conexionMqtt() }).enviarDatos({
    topic: 'motehu/actualizar',
    payload,
  });
};

export default avisarFrontActualizarDatos;
