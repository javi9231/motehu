import Dispositivo from '../components/dispositivo/modelo';

const actualizaUbicacionEnDispositivo = async ({
  nombreUbicacion,
  idDispositivo,
}) => {
  const updateDispositivoError = (error, dispositivoError) => {
    if (error || dispositivoError?.length === 0)
      throw new Error(
        'No ha sido posible actualizaUbicacionEnDispositivo',
        error,
      );
  };

  try {
    const dispositivoEncontrado = await Dispositivo.findOne(
      {
        idDispositivo: idDispositivo,
      },
      updateDispositivoError,
    );
    // no encuentra el dispositivo se sale
    if (!dispositivoEncontrado) return false;
    dispositivoEncontrado.ubicacion = nombreUbicacion;
    return dispositivoEncontrado.save();
  } catch (error) {
    updateDispositivoError(error);
  }
};

export default actualizaUbicacionEnDispositivo;
