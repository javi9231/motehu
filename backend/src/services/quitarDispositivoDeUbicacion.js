import Dispositivo from '../components/dispositivo/modelo';
import Ubicacion from '../components/ubicacion/modelo';
/**
 * Quita un dispositivo a una ubicacion
 * {
 * nombre: "ubicacion",
 * idDispositivo: "232323"
 * }
 *
 */
const quitarDispositivoDeUbicacion = async ({ idDispositivo }) => {
  const dispositivo = await Dispositivo.findOne({ idDispositivo });
  if (!dispositivo) return false;

  const ubicacion = await Ubicacion.findOne({ nombre: dispositivo.ubicacion });

  if (!ubicacion) return false;
  // filtramos por el idDispositivo, solamente se devuelven los dispositivos
  // que no coincidan con el idDispositivo que queremos quitar
  const dispositivosCorrectos = await ubicacion.dispositivos.filter(
    (dispositivo) => {
      dispositivo.idDispositivo !== idDispositivo;
    },
  );
  // Si no tiene dispositivos se desactiva la ubicacion
  if (ubicacion.dispositivos.length === 0) {
    ubicacion.activa = false;
  } else {
    ubicacion.activa = true;
  }

  return Ubicacion.updateOne(
    { nombre: ubicacion.nombre },
    { activa: ubicacion.activa, dispositivos: dispositivosCorrectos },
  );
};

export default quitarDispositivoDeUbicacion;
