import Ubicacion from '../components/ubicacion/modelo';
import Dispositivo from '../components/dispositivo/modelo';

const updateDispositivoError = (error, dispositivo) => {
  if (error || !dispositivo || dispositivo.length === 0)
    throw new Error('No ha sido posible agregarDispositivoEnUbicacion', error);
};
/**
 * Agrega un dispositivo a una ubicacion
 * {
 * nombre: "ubicacion",
 * idDispositivo: "232323"
 * }
 *
 */
const agregarDispositivoEnUbicacion = async ({
  nombreUbicacion,
  idDispositivo,
}) => {
  try {
    const ubicacion = await Ubicacion.findOne({ nombre: nombreUbicacion });
    console.log('ubicacion ', ubicacion);
    if (!idDispositivo) {
      console.log('error no hay dispositivo agregarDispositivoEnUbicacion');
      return null;
    }
    const dispositivo = await Dispositivo.findOne(
      { idDispositivo },
      updateDispositivoError,
    );
    console.log('dispoosidsito', dispositivo);
    // Si no encuentra la ubicacion o el dispositivo
    // algo no esta bien y devuelve un null
    if (!ubicacion || !dispositivo) return null;

    let existeElDispositivoEnLaUbicacion = false;
    // comprobamos si la ubicacion tiene dispositivos
    if (ubicacion.dispositivos) {
      // buscamos el dispositivo en la ubicacion
      existeElDispositivoEnLaUbicacion = ubicacion.dispositivos.some(
        (idDispositivo) => idDispositivo === dispositivo.idDispositivo,
      );
    }
    if (!existeElDispositivoEnLaUbicacion || !ubicacion.dispositivos) {
      ubicacion.dispositivos.push(dispositivo.idDispositivo);
    }
    // al agregar un dispositivo, activamos la ubicacion
    ubicacion.activa = true;
    return ubicacion.save();
  } catch (error) {
    console.log('error agregarDispositivoEnUbicacion ', error);
  }
};

export default agregarDispositivoEnUbicacion;
