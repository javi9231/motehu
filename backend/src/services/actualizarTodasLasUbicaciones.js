import Dispositivo from '../components/dispositivo/modelo';
import Ubicacion from '../components/ubicacion/modelo';

const actualizarTodasLasUbicaciones = async () => {
  //obtenemos todas las ubicaciones que esten activas
  const todasLasUbicacionesActivas = await Ubicacion.find({ activa: true });
  const todosLosDispositivos = await Dispositivo.find({});
  todasLasUbicacionesActivas.map(async (ubicacion) => {
    if (ubicacion.dispositivos.length !== 0) {
      const dispositivoQueSiEstan = ubicacion.dispositivos.filter(
        async (idDispositivo) =>
          todosLosDispositivos.some(
            (dis) =>
              dis.idDispositivo === idDispositivo &&
              dis.ubicacion === ubicacion.nombre,
          ),
      );
      ubicacion.dispositivos = dispositivoQueSiEstan;
    } else {
      ubicacion.activa = false;
    }
    await ubicacion.save();
  });
};

export default actualizarTodasLasUbicaciones;
