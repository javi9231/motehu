import Dispositivo from '../components/dispositivo/modelo';
import { crear as crearDispositivo } from '../components/dispositivo/controlador';
import { crear as crearUbicacion } from '../components/ubicacion/controlador';
import medicion from '../components/medicion/controlador';
import avisarFrontActualizarDatos from './avisarFrontActualizarDatos';

const nuevaMedicion = async (modelo, idDispositivo, mensaje) => {
  try {
    let dispositivo = await Dispositivo.findOne({
      idDispositivo: idDispositivo,
    });
    // si no encuentra el dispositivo, lo crea
    // ademas crea la ubicacion y agrega el dispositivo
    if (!dispositivo) {
      dispositivo = await crearDispositivo.dispositivo(idDispositivo, modelo);
      await crearUbicacion.ubicacionInicial({
        nombre: idDispositivo,
        dispositivos: [dispositivo.idDispositivo],
      });
      avisarFrontActualizarDatos({
        payload: {
          ubicacion: true,
          dispositivos: true,
        },
      });
    }

    await medicion.crear({
      idDispositivo,
      ubicacion: dispositivo.ubicacion,
      temperatura: mensaje.temp,
      humedad: mensaje.hum,
    });
  } catch (error) {
    console.error(
      'Error al actualizar Dispositivos con sus Mediciones ',
      error,
    );
  }
};

export default nuevaMedicion;
