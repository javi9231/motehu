import Dispositivo from '../components/dispositivo/modelo';
import Medicion from '../components/medicion/modelo';

const actualizarDispositivoMediciones = async (id) => {
  try {
    const mediciones = await Medicion.find({ id });
    const res = await Dispositivo.findOneAndUpdate(
      { id },
      {
        mediciones,
      },
    );
    console.log('actualizarDispositivoMediciones', res);
  } catch (error) {
    console.error(
      'Error al actualizar Dispositivos con sus Mediciones ',
      error,
    );
  }
};

export { actualizarDispositivoMediciones };
