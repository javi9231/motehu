import quitarDispositivoDeUbicacion from './quitarDispositivoDeUbicacion';
import actualizaUbicacionEnDispositivo from './actualizaUbicacionEnDispositivo';
import agregarDispositivoEnUbicacion from './agregarDispositivoEnUbicacion';
import avisarFrontActualizarDatos from './avisarFrontActualizarDatos';
import actualizarTodasLasUbicaciones from './actualizarTodasLasUbicaciones';
import Ubicacion from '../components/ubicacion/modelo';

const cambiarDispositivoDeUbicacion = async ({
  idDispositivo,
  nombreUbicacion,
}) => {
  try {
    // lo quitamos de su ubicacion
    await quitarDispositivoDeUbicacion({
      idDispositivo,
    });
    await agregarDispositivoEnUbicacion({
      nombreUbicacion,
      idDispositivo,
    });
    await actualizaUbicacionEnDispositivo({
      nombreUbicacion,
      idDispositivo,
    });
    await actualizarTodasLasUbicaciones();

    // mandar un mensaje de actualizacion al front
    avisarFrontActualizarDatos({
      payload: {
        ubicacion: true,
        dispositivos: true,
      },
    });
  } catch (error) {
    console.log('error cambiarDispositivoDeUbicacion', error);
  }
};

export default cambiarDispositivoDeUbicacion;
