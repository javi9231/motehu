import Dispositivo from '../components/dispositivo/modelo';

const actualizarMedicionEnDispositivo = async (medicion) => {
  const updateDispositivoError = (error, dispositivo) => {
    if (error || dispositivo.length === 0)
      throw new Error('No ha sido posible actualizar el dispositivo');
  };
  try {
    const res1 = await Dispositivo.findOne(
      {
        idDispositivo: medicion.idDispositivo,
      },
      updateDispositivoError,
    );
    res1.mediciones.push(medicion._id);
    const res = await res1.save();
    console.log('actualizarMedicionEnDispositivo', res);
  } catch (error) {
    console.error(
      'Error al actualizar Dispositivos con sus Mediciones ',
      error,
    );
  }
};

export default actualizarMedicionEnDispositivo;
