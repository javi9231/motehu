const path = require('path');
const Dotenv = require('dotenv-webpack'); // para leer archivos env
const webpack = require('webpack'); // to access built-in plugins
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

module.exports = (env) => {
  const isProduction = env.production || false;
  return {
    resolve: {
      extensions: ['.web.js', '.jsx', '.js', '.json'],
      aliasFields: ['browser'],
    },
    devServer: {
      contentBase: path.join(__dirname, 'public'),
      compress: true,
      port: 3000,
      historyApiFallback: true,
    },
    entry: path.resolve(__dirname, 'src', 'index.jsx'),
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'bundle.js',
      publicPath: '/',
    },
    module: {
      rules: [
        {
          test: /\.(jsx|js)$/,
          exclude: /node_modules/,
          use: [{ loader: 'babel-loader' }, { loader: 'eslint-loader' }],
        },
        {
          test: /\.css$/,
          use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
        },
        {
          test: /\.(png|svg|jpg|gif)$/i,
          use: ['file-loader'],
        },
        {
          enforce: 'pre',
          test: /\.js$/,
          loader: 'source-map-loader',
        },
      ],
    },
    plugins: [
      new Dotenv({
        path: `./.env${env.file ? `.${env.file}` : ''}`, // Path to .env file (this is the default)
        safe: true, // load .env.example (defaults to "false" which does not use dotenv-safe)
      }),
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
      new LodashModuleReplacementPlugin({
        collections: true,
        paths: true,
        shorthands: true,
      }),
    ],
    devtool: isProduction ? 'source-map' : 'inline-source-map',
  };
};
