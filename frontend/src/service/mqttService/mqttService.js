import mqtt from 'mqtt';

const options = {
  protocol: 'ws',
  keepalive: 30,
  port: 1888,
  // clientId uniquely identifies client
  // choose any string you wish
  clientId: 'frontend2345234',
};
const client = mqtt.connect('ws://193.168.0.15', options);

client.subscribe('/demo');

const clienteMsg = (callBack) => {
  client.on('message', (topic, message) => {
    const note = message.toString();
    // Updates React state with message
    callBack(note);
    console.log(note);
    client.end();
  });
};

export default { clienteMsg };
