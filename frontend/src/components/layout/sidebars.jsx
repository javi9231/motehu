import React from 'react';
import { Menu } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import {
  DashboardOutlined,
  HistoryOutlined,
  HomeOutlined,
  SettingOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { useHistory } from 'react-router';
import { routes } from '../../config/routes';

const SideNav = () => {
  const history = useHistory();

  const handleMonitorClick = () => {
    history.push(routes.monitor);
  };

  const handleHistoricoClick = () => {
    history.push(routes.historico);
  };

  const handleUsuariosClick = () => {
    history.push(routes.configUsuarios);
  };
  const handleUbicacionClick = () => {
    history.push(routes.configUbicacion);
  };

  return (
    <div>
      <div
        style={{
          height: '32px',
          background: 'rgba(255, 255, 255, 0.2)',
          margin: '16px',
        }}
      />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['0']}>
        <Menu.Item
          key="1"
          icon={<DashboardOutlined />}
          onClick={handleMonitorClick}
        >
          Monitor
        </Menu.Item>
        <Menu.Item
          key="2"
          icon={<HistoryOutlined />}
          onClick={handleHistoricoClick}
        >
          Historico
        </Menu.Item>
        <SubMenu title="Configuración" icon={<SettingOutlined />}>
          <Menu.Item
            key="3"
            icon={<HomeOutlined />}
            onClick={handleUbicacionClick}
          >
            UbicacionConfig
          </Menu.Item>
          <Menu.Item
            key="5"
            icon={<UserOutlined />}
            onClick={handleUsuariosClick}
          >
            Usuarios
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="6" icon={<UserOutlined />}>
          Salir
        </Menu.Item>
      </Menu>
    </div>
  );
};

export default SideNav;
