/* eslint-disable react/jsx-props-no-spreading */
import { Button, Col, Drawer, Form, Input, Row, message, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUbicaciones } from '../../slices/ubicaciones';
import { ubicaciones } from '../../api';

const UbicacionNueva = ({ mostrarDrawer, setMostrarDrawer }) => {
  const ID_MONGO = 'idDispositivo';
  const TITULO = 'NUEVA UBICACION';
  const onClose = () => setMostrarDrawer(false);
  const dispatch = useDispatch();
  const { Option } = Select;
  const [form] = Form.useForm();

  const todosLosDispositivos = useSelector(
    (state) => state.dispositivos.dispositivos
  );
  const [valueLabelDispositivos, setValueLabelDispositivos] = useState([]);

  useEffect(() => {
    const valueLabelDispositivosTemp = [];

    Object.keys(todosLosDispositivos).forEach((key) => {
      valueLabelDispositivosTemp.push({
        value: todosLosDispositivos[key][ID_MONGO],
        label: todosLosDispositivos[key].nombre,
      });
    });
    setValueLabelDispositivos(valueLabelDispositivosTemp);
  }, [todosLosDispositivos]);

  const nuevaUbicacion = async ({ nombreUbicacion, dispositivosUbicacion }) => {
    try {
      const datos = {
        nombreUbicacion,
        dispositivosUbicacion,
      };
      console.log('nueva ubicacion', datos);
      await ubicaciones.crear.ubicacion(datos);
    } catch (error) {
      message.error('No se ha podido crear la ubicación ');
    }
  };

  const handleSubmit = async (data) => {
    console.log('data on save ', data);
    // TODO grabar cambios ubicacion
    try {
      nuevaUbicacion(data);
      // actualizamos ubicaciones Redux
      dispatch(fetchUbicaciones());
      onClose();
    } catch (error) {
      message.error('Error en ubicación ');
    }
  };

  const Footer = (
    <div
      style={{
        textAlign: 'right',
      }}
    >
      <Button onClick={onClose} style={{ marginRight: 8 }}>
        Cancelar
      </Button>
      <Button onClick={form.submit} type="primary">
        Grabar
      </Button>
    </div>
  );

  return (
    <Drawer
      title={TITULO}
      onClose={onClose}
      visible={mostrarDrawer}
      bodyStyle={{ paddingBottom: 80 }}
      footer={Footer}
    >
      <Form
        form={form}
        layout="vertical"
        onFinish={handleSubmit}
        hideRequiredMark
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              name="nombreUbicacion"
              label="Nombre de la ubicación:"
              rules={[
                {
                  required: true,
                  message: 'Nombre ubicacion',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="dispositivosUbicacion"
              label="Dispositivos en la ubicación"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'Selecciona un dispositivo',
                },
              ]}
            >
              <Select
                mode="multiple"
                style={{ width: '100%' }}
                placeholder="Selecciona dispositivo"
                optionLabelProp="label"
              >
                {valueLabelDispositivos &&
                  valueLabelDispositivos.map(({ value, label }) => (
                    <Option value={value} label={label} key={value}>
                      <div className="demo-option-label-item">{label}</div>
                    </Option>
                  ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default UbicacionNueva;
