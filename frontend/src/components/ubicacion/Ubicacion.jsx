import { Button, Col, Space } from 'antd';
import React, { useState } from 'react';
import { SettingTwoTone } from '@ant-design/icons';
import Dispositivo from '../dispositivo';
import UbicacionEditar from './UbicacionEditar';

const Ubicacion = ({
  nombreUbicacion,
  dispositivos,
  activa,
  mostrarTodas = false,
}) => {
  const [mostrarDrawer, setMostrarDrawer] = useState(false);
  const esEditable = true;

  const handleEditOnClick = () => {
    setMostrarDrawer(true);
  };
  // Si no esta activa no se muestra la ubicación
  if (!activa && !mostrarTodas) return null;

  return (
    <>
      <Col span={19}>
        <strong>Ubicacion: </strong>
        {nombreUbicacion}
      </Col>
      <Col span={1}>
        {esEditable && (
          <Button
            type="link"
            icon={<SettingTwoTone />}
            onClick={handleEditOnClick}
          />
        )}
      </Col>
      <Space wrap>
        {dispositivos.map((idDispositivo) => (
          <Dispositivo
            key={Math.random()}
            idDispositivo={idDispositivo}
            ubicacion={nombreUbicacion}
          />
        ))}
      </Space>
      {mostrarDrawer && (
        <UbicacionEditar
          mostrarDrawer={mostrarDrawer}
          setMostrarDrawer={setMostrarDrawer}
          nombreUbicacion={nombreUbicacion}
          dispositivos={dispositivos}
        />
      )}
    </>
  );
};

export default Ubicacion;
