/* eslint-disable react/jsx-props-no-spreading */
import { Button, Col, Drawer, Form, Input, Row, message, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUbicaciones } from '../../slices/ubicaciones';
import { ubicaciones } from '../../api';

const UbicacionEditar = ({
  mostrarDrawer,
  setMostrarDrawer,
  nombreUbicacion,
  dispositivos,
}) => {
  const idMongo = 'idDispositivo';
  const TITULO = 'EDITAR UBICACIÓN';
  const onClose = () => setMostrarDrawer(false);
  const dispatch = useDispatch();
  const { Option } = Select;
  const [form] = Form.useForm();
  const [valueLabelDispositivos, setValueLabelDispositivos] = useState([]);

  const todosLosDispositivos = useSelector(
    (state) => state.dispositivos.dispositivos
  );

  const [dispositivosUbicacionInicio, setDispositivosUbicacionInicio] =
    useState();

  useEffect(() => {
    if (!dispositivos) return;
    setDispositivosUbicacionInicio(dispositivos);
  }, []);

  useEffect(() => {
    const valueLabelDispositivosTemp = [];

    Object.keys(todosLosDispositivos).forEach((key) => {
      valueLabelDispositivosTemp.push({
        value: todosLosDispositivos[key][idMongo],
        label: todosLosDispositivos[key].nombre,
      });
    });
    setValueLabelDispositivos(valueLabelDispositivosTemp);
  }, []);

  const editarUbicacion = async (data) => {
    try {
      // Cambiamos el nombre, si viene nuevo nombre
      await ubicaciones.actualizar.ubicacion({
        nombreViejo: nombreUbicacion.toLowerCase(),
        nombreNuevo: data.nombre.toLowerCase(),
        dispositivosUbicacion: data.dispositivosUbicacion,
      });
    } catch (error) {
      message.error('No se ha podido actualizar el nombre de la ubicación ');
    }
  };

  const handleSubmit = async (data) => {
    // TODO grabar cambios ubicacion
    try {
      editarUbicacion(data);
      // actualizamos ubicaciones Redux
      dispatch(fetchUbicaciones());
      onClose();
    } catch (error) {
      message.error('Error en ubicación ');
    }
  };

  const Footer = (
    <div
      style={{
        textAlign: 'right',
      }}
    >
      <Button onClick={onClose} style={{ marginRight: 8 }}>
        Cancelar
      </Button>
      <Button onClick={form.submit} type="primary">
        Grabar
      </Button>
    </div>
  );

  return (
    <Drawer
      title={TITULO}
      onClose={onClose}
      visible={mostrarDrawer}
      bodyStyle={{ paddingBottom: 80 }}
      footer={Footer}
    >
      {dispositivosUbicacionInicio && (
        <Form
          form={form}
          layout="vertical"
          onFinish={handleSubmit}
          initialValues={{
            nombre: nombreUbicacion,
            dispositivosUbicacion: dispositivosUbicacionInicio,
          }}
          hideRequiredMark
        >
          <Row gutter={24}>
            <Col span={24}>
              <strong>{nombreUbicacion}:</strong>
              <Form.Item name="nombre" label="Nombre ubicación:">
                <Input />
              </Form.Item>
              <Form.Item
                name="dispositivosUbicacion"
                label="Dispositivos en la ubicación"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Selecciona un dispositivo',
                  },
                ]}
              >
                <Select
                  mode="multiple"
                  style={{ width: '100%' }}
                  placeholder="Selecciona dispositivo"
                  optionLabelProp="label"
                >
                  {valueLabelDispositivos &&
                    valueLabelDispositivos.map(({ value, label }) => (
                      <Option value={value} label={label} key={value}>
                        <div className="demo-option-label-item">{label}</div>
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      )}
    </Drawer>
  );
};

export default UbicacionEditar;
