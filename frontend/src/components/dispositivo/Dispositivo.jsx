import { Card, Progress } from 'antd';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
// import { dispositivoSelector } from '../../slices/dispositivos';

const Dispositivo = ({ idDispositivo, ubicacion }) => {
  // Iniacializar los hooks redux
  const dispositivos = useSelector((state) => state.dispositivos.dispositivos);
  const mediciones = useSelector((state) => state.mediciones.dispositivos);
  const [temp, setTemp] = useState('-');
  const [hum, setHum] = useState('-');

  useEffect(() => {
    if (!idDispositivo) return;
    const dispositivo = _.find(
      dispositivos,
      (d) => d.idDispositivo === idDispositivo
    );
    if (!dispositivo || _.isEmpty(mediciones)) return;
    setTemp(mediciones[dispositivo.idDispositivo]?.temp);
    setHum(mediciones[dispositivo.idDispositivo]?.hum);
  }, [mediciones]);

  // Para obtener una escala de -50 a +50 grados en la grafica
  const tempPercent = temp + 50;
  return (
    <div className="site-card-border-less-wrapper">
      <Card title={`Dispositivo: ${idDispositivo}`} style={{ width: 300 }}>
        <div>
          <strong>Ubicacion: </strong>
          {ubicacion}
        </div>
        <div>
          <Progress
            type="circle"
            percent={tempPercent}
            format={(percent) => (percent > 0 ? `${percent - 50}º` : '-º')}
            strokeColor={{
              '0%': '#00ccff',
              '100%': '#ff0000',
            }}
            strokeWidth="10"
          />
          <Progress
            type="circle"
            percent={hum}
            format={(percent) => `${percent} %`}
            strokeColor={{
              '0%': '#108ee9',
              '100%': '#87d068',
            }}
            strokeWidth="10"
          />
        </div>
      </Card>
    </div>
  );
};
export default Dispositivo;
