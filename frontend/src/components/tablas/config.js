import React from 'react';
import moment from 'moment';

const columns = [
  {
    key: 'idDispositivo',
    title: 'ID',
    dataIndex: 'idDispositivo',
    // width: 150,
  },
  {
    key: 'ubicacion',
    title: 'Ubicacion',
    dataIndex: 'ubicacion',
    // width: 150,
  },
  {
    key: 'humedad',
    title: 'humedad',
    dataIndex: 'humedad',
    // width: 150,
  },
  {
    key: 'temperatura',
    title: 'temperatura',
    dataIndex: 'temperatura',
  },
  {
    key: 'fecha',
    title: 'fecha',
    dataIndex: 'createdAt',
    render: (fecha) => <>{moment(fecha).format('hh:mm DD-MM-YYYY')}</>,
  },
];

export { columns };
