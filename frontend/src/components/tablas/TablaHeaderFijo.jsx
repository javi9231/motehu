import React from 'react';
import { Table } from 'antd';
import { columns } from './config';

const TablaHeaderFijo = ({ datos, loading }) => {
  return (
    <Table
      columns={columns}
      loading={loading}
      dataSource={datos}
      scroll={{ y: 640 }}
    />
  );
};

export default TablaHeaderFijo;
