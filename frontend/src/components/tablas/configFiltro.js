import React from 'react';
import moment from 'moment';

const columns = [
  {
    key: 'idDispositivo',
    title: 'ID',
    dataIndex: 'idDispositivo',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.idDispositivo - b.idDispositivo,
  },
  {
    key: 'ubicacion',
    title: 'Ubicacion',
    dataIndex: 'ubicacion',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.ubicacion - b.ubicacion,
  },
  {
    key: 'humedad',
    title: 'humedad',
    dataIndex: 'humedad',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.humedad - b.humedad,
  },
  {
    key: 'temperatura',
    title: 'temperatura',
    dataIndex: 'temperatura',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.temperatura - b.temperatura,
  },
  {
    key: 'fecha',
    title: 'fecha',
    dataIndex: 'createdAt',
    render: (fecha) => <>{moment(fecha).format('hh:mm DD-MM-YYYY')}</>,
  },
];

export { columns };
