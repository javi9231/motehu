import { Typography } from 'antd';
import React, { useState } from 'react';
import TablaHeaderFiltro from '../../tablas/TablaHeaderFiltro';
import FiltroHistorico from './FiltroHistorico';
import llamadas from './llamadasFiltro';

const { Title } = Typography;
const Historico = () => {
  const [datos, setDatos] = useState([]);
  const [loading, setLoading] = useState(false);

  const onFinishForm = async ({
    nombreUbicacion,
    idDispositivo,
    intervaloFechas,
  }) => {
    if (!nombreUbicacion && !idDispositivo) return;
    console.log('form data ', nombreUbicacion, idDispositivo, intervaloFechas);
    setLoading(true);
    const fechaInicio = intervaloFechas[0].toISOString(); // .valueOf();
    const fechaFin = intervaloFechas[1].toISOString(); // .valueOf();
    try {
      if (nombreUbicacion && idDispositivo) {
        const res = await llamadas.ubicacionDispositivo({
          nombreUbicacion,
          idDispositivo,
          fechaInicio,
          fechaFin,
        });
        setDatos(res);
      } else if (nombreUbicacion && !idDispositivo) {
        const res = await llamadas.ubicacion({
          nombreUbicacion,
          fechaInicio,
          fechaFin,
        });
        setDatos(res);
      } else if (!nombreUbicacion && idDispositivo) {
        const res = await llamadas.dispositivo({
          idDispositivo,
          fechaInicio,
          fechaFin,
        });
        setDatos(res);
      }
    } catch (error) {
      console.error(error);
    }
    setLoading(false);
  };
  return (
    <div>
      <FiltroHistorico onFinishForm={onFinishForm} />
      <Title> Historico</Title>
      <TablaHeaderFiltro loading={loading} datos={datos} />
    </div>
  );
};

export default Historico;
