import { Button, Form, Input, DatePicker, Select } from 'antd';
import { Option } from 'antd/lib/mentions';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUbicaciones } from '../../../slices/ubicaciones';

const FiltroHistorico = ({ onFinishForm }) => {
  const [form] = Form.useForm();
  const { RangePicker } = DatePicker;
  const dispatch = useDispatch();
  const todosLasUbicaciones = useSelector(
    (state) => state.ubicaciones.ubicaciones
  );
  const [valueLabelUbicaciones, setValueLabelUbicaciones] = useState();
  useEffect(() => {
    // actualizamos ubicaciones Redux
    dispatch(fetchUbicaciones());
  }, []);

  useEffect(() => {
    const valueLabelUbicacionesTemp = [];

    Object.keys(todosLasUbicaciones).forEach((key) => {
      valueLabelUbicacionesTemp.push({
        value: todosLasUbicaciones[key].nombre,
        label: todosLasUbicaciones[key].nombre,
      });
    });
    setValueLabelUbicaciones(valueLabelUbicacionesTemp);
  }, [todosLasUbicaciones]);

  return (
    <>
      <Form form={form} layout="inline" onFinish={onFinishForm}>
        <Form.Item name="nombreUbicacion" label="Ubicacion">
          <Select
            style={{ width: '100%' }}
            placeholder="Selecciona ubicación"
            optionLabelProp="label"
            allowClear
          >
            {valueLabelUbicaciones &&
              valueLabelUbicaciones.map(({ value, label }) => (
                <Option value={value} label={label} key={value}>
                  <div className="demo-option-label-item">{label}</div>
                </Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item name="idDispositivo" label="Id dispositivo">
          <Input placeholder="Introduzca el id del dispositivo" />
        </Form.Item>
        <Form.Item
          name="intervaloFechas"
          rules={[
            {
              required: true,
              message: 'Seleccione una fecha',
            },
          ]}
        >
          <RangePicker showTime />
        </Form.Item>
        <Form.Item>
          <Button type="primary" onClick={form.submit}>
            Filtrar
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default FiltroHistorico;
