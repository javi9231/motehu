import mediciones from '../../../api/mediciones';

const llamadas = {
  ubicacion: mediciones.leer.nombreUbicacion,
  dispositivo: mediciones.leer.dispositivo,
  ubicacionDispositivo: mediciones.leer.ubicacionDispositivo,
};
export default llamadas;
