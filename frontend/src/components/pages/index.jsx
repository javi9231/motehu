import { configureStore } from '@reduxjs/toolkit';
import 'antd/dist/antd.css';
import React from 'react';
import { Provider } from 'react-redux';
import ApplicationRoutes from '../../config/ApplicationRoutes';
import Mqtt from '../../mqtt/Mqtt';
import rootReducer from '../../slices';

const store = configureStore({ reducer: rootReducer });
const App = () => (
  <Provider store={store}>
    <Mqtt endpoint="ws://193.168.0.15" port="1888" topicSubscribe="motehu/#" />
    <ApplicationRoutes />
  </Provider>
);

export default App;
