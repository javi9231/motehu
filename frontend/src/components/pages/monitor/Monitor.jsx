import React, { useEffect, useState } from 'react';
import { Button, Row, Space } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { PlusCircleOutlined } from '@ant-design/icons';
import {
  fetchUbicaciones,
  ubicacionSelector,
} from '../../../slices/ubicaciones';
import { fetchDispositivos } from '../../../slices/dispositivos';
import Ubicacion from '../../ubicacion/Ubicacion';
import UbicacionNueva from '../../ubicacion/UbicacionNueva';
// import './monitor.css';

const Monitor = () => {
  // mostrar nueva ubicacion
  const [mostrarNuevaUbicacion, setMostrarNuevaUbicacion] = useState(false);
  // Iniacializar los hooks redux
  const { hasErrorsUbicacion, ubicaciones } = useSelector(ubicacionSelector);
  // Iniacializar los hooks redux
  const dispatch = useDispatch();

  useEffect(() => {
    // cargamos las ubicacione del back
    dispatch(fetchUbicaciones());
    // cargamos los dispositivos del back
    dispatch(fetchDispositivos());
  }, []);

  const handleNuevaUbicacion = () => {
    setMostrarNuevaUbicacion(true);
  };
  return (
    <>
      <div className="monitor_dispositivos__contenedor">
        <Space wrap>
          {ubicaciones &&
            Array.isArray(ubicaciones) &&
            ubicaciones.map((data) => {
              return (
                <Row key={Math.random()}>
                  <Ubicacion
                    nombreUbicacion={data.nombre}
                    dispositivos={data.dispositivos}
                    activa={data.activa}
                  />
                </Row>
              );
            })}
          <Button
            type="link"
            icon={<PlusCircleOutlined style={{ fontSize: '32px' }} />}
            size="large"
            onClick={handleNuevaUbicacion}
          />

          {hasErrorsUbicacion && (
            <>
              <div>Error sin conexión</div>
              <div>add u</div>
            </>
          )}

          <UbicacionNueva
            mostrarDrawer={mostrarNuevaUbicacion}
            setMostrarDrawer={setMostrarNuevaUbicacion}
          />
        </Space>
      </div>
    </>
  );
};

export default Monitor;
