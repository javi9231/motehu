import React from 'react';
import { render } from 'react-dom';
import App from './components/pages';

render(<App />, document.getElementById('root'));
