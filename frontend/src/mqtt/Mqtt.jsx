import React, { useEffect } from 'react';
import mqtt from 'mqtt';
import { useDispatch } from 'react-redux';
import medicionMqtt from './medicionMqtt';
import actualizarMqtt from './actualizarMqtt';

const Mqtt = ({ endpoint, port, topicSubscribe }) => {
  // Iniacializar los hooks redux
  const dispatch = useDispatch();

  // mqtt
  let client = null;
  // Identificamos a cada cliente web con un id aleatorio
  const clientId = `mqttjs_${Math.random().toString(16).substr(2, 8)}`;
  const options = {
    protocol: 'ws',
    // keepalive: 30,
    port,
    // clientId unica
    clientId,
  };

  const gestionDeMensajesMQTT = {
    esp32: medicionMqtt,
    actualizar: actualizarMqtt,
  };

  useEffect(() => {
    client = mqtt.connect(endpoint, options);
    client.on('connect', () => {
      client.subscribe(topicSubscribe);
      console.log('Conectado ', clientId);
    });

    client.on('message', (topicMqtt, messageMqtt) => {
      const [, paraQuien, id, accion] = topicMqtt.split('/');
      console.log('quien ', paraQuien);
      // Depende de para quien se mande el mensaje
      // si esta contemplado se actuara en concordancia
      if (gestionDeMensajesMQTT[paraQuien]) {
        gestionDeMensajesMQTT[paraQuien]({ messageMqtt, accion, id, dispatch });
      }
    });

    client.on('close', () => {
      console.log(' disconnected');
    });
    return () => {
      client.end();
    };
  }, []);

  return <div />;
};

export default Mqtt;
