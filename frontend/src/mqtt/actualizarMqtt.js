import { fetchDispositivos } from '../slices/dispositivos';
import { fetchUbicaciones } from '../slices/ubicaciones';

const accionMensaje = (dispatch) => ({
  ubicacion: () => dispatch(fetchUbicaciones()),
  dispositivos: () => dispatch(fetchDispositivos()),
});
const actualizarMqtt = ({ messageMqtt, dispatch }) => {
  const mensaje = JSON.parse(messageMqtt.toString());
  console.log('mensaje mqtt', mensaje);
  Object.keys(mensaje).forEach((accion) => {
    accionMensaje(dispatch)[accion]();
  });
};

export default actualizarMqtt;
