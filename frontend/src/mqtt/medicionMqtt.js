import { addMedicionRedux } from '../slices/mediciones';

const medicionMqtt = ({ messageMqtt, accion, id, dispatch }) => {
  // Comprobamos si el dispositivo existe
  let medicion = null;
  try {
    medicion = JSON.parse(messageMqtt.toString());
  } catch (error) {
    if (accion === 'conectado') {
      medicion = {
        conectado: true,
      };
    } else if (!medicion.temp) {
      return;
    }
  }
  const dispositivoMedicionEnRedux = {
    [id]: {
      ...medicion,
    },
  };

  dispatch(addMedicionRedux(dispositivoMedicionEnRedux));
};

export default medicionMqtt;
