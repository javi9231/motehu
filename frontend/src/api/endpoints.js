const url = {
  dispositivo: 'api/v1/dispositivo',
  medicion: 'api/v1/medicion',
  ubicacion: 'api/v1/ubicacion',
};
const DOMAIN = process.env.REACT_APP_DOMAIN;
const getEndpoints = (ruta) => `${DOMAIN}${url[ruta]}`;
export { getEndpoints };
