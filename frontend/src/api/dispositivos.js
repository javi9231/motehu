import { getWrapper } from './wrapper';
import { getEndpoints } from './endpoints';

// Leer
const obtenerTodosLosDispositivos = () =>
  getWrapper({
    endpoint: `${getEndpoints('dispositivo')}/all`,
  });

const leer = {
  todos: obtenerTodosLosDispositivos,
};

export default { leer };
