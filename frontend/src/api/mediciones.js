import { getWrapper } from './wrapper';
import { getEndpoints } from './endpoints';

// Leer
const obtenerTodasLasMediciones = () =>
  getWrapper({
    endpoint: `${getEndpoints('medicion')}/all`,
  });

const obtenerNombreUbicacion = ({ nombreUbicacion, fechaInicio, fechaFin }) =>
  getWrapper({
    endpoint: `${getEndpoints(
      'medicion'
    )}/ubicacion/${nombreUbicacion}/${fechaInicio}/${fechaFin}`,
  });
const obtenerDispositivo = ({ idDispositivo, fechaInicio, fechaFin }) =>
  getWrapper({
    endpoint: `${getEndpoints(
      'medicion'
    )}/idDispositivo/${idDispositivo}/${fechaInicio}/${fechaFin}`,
  });

const obtenerUbicacionDispositivo = ({
  nombreUbicacion,
  idDispositivo,
  fechaInicio,
  fechaFin,
}) =>
  getWrapper({
    endpoint: `${getEndpoints(
      'medicion'
    )}/ubicacion_idDispositivo/${nombreUbicacion}/${idDispositivo}/${fechaInicio}/${fechaFin}`,
  });

const leer = {
  todos: obtenerTodasLasMediciones,
  nombreUbicacion: obtenerNombreUbicacion,
  dispositivo: obtenerDispositivo,
  ubicacionDispositivo: obtenerUbicacionDispositivo,
};

export default { leer };
