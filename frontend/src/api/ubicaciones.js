import { postWrapper, getWrapper, putWrapper } from './wrapper';
import { getEndpoints } from './endpoints';

// Crear
const nuevaUbicacion = ({ nombreUbicacion, dispositivosUbicacion }) =>
  postWrapper({
    endpoint: `${getEndpoints('ubicacion')}/crear`,
    data: { nombreUbicacion, dispositivosUbicacion },
  });

const crear = {
  ubicacion: nuevaUbicacion,
};
// Leer
const obtenerTodasLasUbicaciones = () =>
  getWrapper({
    endpoint: `${getEndpoints('ubicacion')}/all`,
  });

const leer = {
  todos: obtenerTodasLasUbicaciones,
};

// Actualizar
const actualizarUbicacion = ({
  nombreViejo,
  nombreNuevo,
  dispositivosUbicacion,
}) => {
  return putWrapper({
    endpoint: `${getEndpoints('ubicacion')}/${nombreViejo}`,
    data: {
      nombre: nombreNuevo || nombreViejo,
      dispositivos: dispositivosUbicacion,
    },
  });
};

const actualizar = {
  ubicacion: actualizarUbicacion,
};

export default { crear, leer, actualizar };
