const getWrapper = ({ endpoint, parameters = {}, headers = {} }) =>
  fetch(endpoint, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      ...headers,
    },
    params: parameters,
    // credentials: 'include',
  }).then((response) => response.json());

const postWrapper = ({ endpoint, data = {}, headers = {} }) =>
  fetch(endpoint, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      ...headers,
    },
    // credentials: 'include',
  });

const putWrapper = ({ endpoint, data = {}, headers = {} }) =>
  fetch(endpoint, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      ...headers,
    },
  });
export { getWrapper, postWrapper, putWrapper };
