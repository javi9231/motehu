/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import ubicacionApi from '../api/ubicaciones';

const initialState = {
  loadingUbicaciones: false,
  hasErrorsUbicacion: false,
  ubicaciones: {},
};

const ubicacionesSlice = createSlice({
  name: 'ubicaciones',
  initialState,
  reducers: {
    getUbicaciones: (state) => {
      state.loadingUbicaciones = true;
    },
    getUbicacionesSuccess: (state, { payload }) => {
      state.ubicaciones = payload;
      state.loadingUbicaciones = false;
      state.hasErrorsUbicacion = false;
    },
    getUbicacionesFailure: (state) => {
      state.loadingUbicaciones = false;
      state.hasErrorsUbicacion = true;
    },
    addUbicacion: (state, { payload }) => {
      const newKey = Object.keys(payload)[0];
      state.ubicaciones = {
        ...state.ubicaciones,
        [newKey]: {
          ...state.ubicaciones[newKey],
          ...payload[newKey],
        },
      };
    },
  },
});

// actions
export const {
  getUbicaciones,
  getUbicacionesSuccess,
  getUbicacionesFailure,
  setUbicacionNombre,
  addUbicacion,
  // updateData,
} = ubicacionesSlice.actions;

// Selector
export const ubicacionSelector = (state) => state.ubicaciones;

// Reducer
export default ubicacionesSlice.reducer;

// Asincrona thunk action
export const fetchUbicaciones = () => (dispatch) => {
  dispatch(getUbicaciones());
  ubicacionApi.leer
    .todos()
    .then((data) => dispatch(getUbicacionesSuccess(data)))
    .catch((error) => {
      dispatch(getUbicacionesFailure(error));
    });
};
