/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import dispositivoApi from '../api/dispositivos';

const initialState = {
  loadingDispositivos: false,
  hasErrorsDispositivos: false,
  dispositivos: {},
};

const dispositivosSlice = createSlice({
  name: 'dispositivos',
  initialState,
  reducers: {
    getDispositivos: (state) => {
      state.loadingDispositivos = true;
    },
    getDispositivosSuccess: (state, { payload }) => {
      payload.forEach((element) => {
        state.dispositivos = {
          ...state.dispositivos,
          [element.idDispositivo]: {
            ...state.dispositivos[element.idDispositivo],
            ...element,
          },
        };
      });
      state.loadingDispositivos = false;
      state.hasErrorsDispositivos = false;
    },
    getDispositivosFailure: (state) => {
      state.loadingDispositivos = false;
      state.hasErrorsDispositivos = true;
    },
    addMedicionDispositivoRedux: (state, { payload }) => {
      const idDispositivo = payload[0].nombre;
      state.dispositivos = {
        ...state.dispositivos,
        [idDispositivo]: {
          ...state.dispositivos[idDispositivo],
          ...payload[0],
        },
      };
    },
    setMedicion: (state, { payload }) => {
      state.dispositivos = {
        ...state.dispositivos,
        ...payload,
      };
    },
  },
});

// actions
export const {
  getDispositivos,
  getDispositivosSuccess,
  getDispositivosFailure,
  addMedicionDispositivoRedux,
  setMedicion,
} = dispositivosSlice.actions;

// Selector
export const dispositivoSelector = (state) => state.dispositivos.dispositivos;

// Reducer
export default dispositivosSlice.reducer;

// Asincrona thunk action
export const fetchDispositivos = () => (dispatch) => {
  dispatch(getDispositivos());
  try {
    dispositivoApi.leer
      .todos()
      .then((data) => dispatch(getDispositivosSuccess(data)));
  } catch (error) {
    dispatch(getDispositivosFailure());
  }
};
