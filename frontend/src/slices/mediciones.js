/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import dispositivoApi from '../api/mediciones';

const initialState = {
  loadingMediciones: false,
  hasErrorsMediciones: false,
  dispositivos: {},
};

const medicionesSlice = createSlice({
  name: 'mediciones',
  initialState,
  reducers: {
    getMediciones: (state) => {
      state.loadingMediciones = true;
    },
    getMedicionesSuccess: (state, { payload }) => {
      payload.forEach((element) => {
        state.dispositivos = {
          ...state.dispositivos,
          [element.idDispositivo]: {
            ...state.dispositivos[element.idDispositivo],
            ...element,
          },
        };
      });
      state.loadingMediciones = false;
      state.hasErrorsMediciones = false;
    },
    getMedicionesFailure: (state) => {
      state.loadingMediciones = false;
      state.hasErrorsMediciones = true;
    },
    addMedicionRedux: (state, { payload }) => {
      const idDispositivo = Object.keys(payload)[0];
      state.dispositivos = {
        ...state.dispositivos,
        [idDispositivo]: {
          ...state.dispositivos[idDispositivo],
          ...payload[idDispositivo],
        },
      };
    },
    setMedicion: (state, { payload }) => {
      state.mediciones = {
        ...state.mediciones,
        ...payload,
      };
    },
  },
});

// actions
export const {
  getMediciones,
  getMedicionesSuccess,
  getMedicionesFailure,
  addMedicionRedux,
  setMedicion,
} = medicionesSlice.actions;

// Selector
export const medicioneSelector = (state) => state.dispositivos;

// Reducer
export default medicionesSlice.reducer;

// Asincrona thunk action
export const fetchMediciones = () => (dispatch) => {
  dispatch(getMediciones());
  try {
    dispositivoApi.leer
      .todos()
      .then((data) => dispatch(getMedicionesSuccess(data)));
  } catch (error) {
    dispatch(getMedicionesFailure());
  }
};
