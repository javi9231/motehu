import { combineReducers } from 'redux';
import dispositivosSlice from './dispositivos';
import ubicacionesSlice from './ubicaciones';
import medicionesSlice from './mediciones';

const rootReducer = combineReducers({
  dispositivos: dispositivosSlice,
  ubicaciones: ubicacionesSlice,
  mediciones: medicionesSlice,
});

export default rootReducer;
