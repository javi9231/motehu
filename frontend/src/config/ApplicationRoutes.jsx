import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { Layout } from 'antd';
import { routes } from './routes';
import Monitor from '../components/pages/monitor/Monitor';
import Historico from '../components/pages/historico/Historico';
import SideNav from '../components/layout/sidebars';
import Login from '../components/pages/login/Login';
import UsuariosConfig from '../components/pages/configuracion/UsuariosConfig';

const { Header, Sider, Content } = Layout;

const ApplicationRoutes = () => {
  return (
    <Router>
      <Layout>
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={(broken) => {
            console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}
        >
          <SideNav />
        </Sider>
        <Layout>
          <Header
            className="site-layout-sub-header-background"
            style={{ padding: 0 }}
          />
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 'calc(100vh - 114px)',
              background: '#fff',
            }}
          >
            <Switch>
              <Route path={routes.monitor} component={Monitor} />
              <Route path={routes.historico} component={Historico} />
              <Route path={routes.configUsuarios} component={UsuariosConfig} />
              <Route path={routes.login} component={Login} />
              <Redirect to={routes.salir} from="/" />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </Router>
  );
};

export default ApplicationRoutes;
