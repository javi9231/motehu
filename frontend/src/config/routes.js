export const routes = {
  salir: '/login',
  monitor: '/monitor',
  historico: '/historico',
  configUbicacion: '/config/ubicacion',
  configDispositivos: '/config/dispositivos',
  configUsuarios: '/config/usuarios',
};
